package com.test.bagussetiadi.domain

import com.test.bagussetiadi.data.model.StoryModel
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import okhttp3.OkHttpClient

import okhttp3.logging.HttpLoggingInterceptor

interface ApiService {

    @GET("topstories.json?print=pretty")
    suspend fun getTopStories() : Response<List<Integer>>

    @GET("item/{id}.json?print=pretty")
    suspend fun getDetailStory(@Path("id") id: String) : Response<StoryModel>

    companion object {
        private var retrofitService: ApiService? = null
        fun getInstance() : ApiService {
            if (retrofitService == null) {

                val interceptor = HttpLoggingInterceptor()
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
                val client: OkHttpClient = OkHttpClient.Builder().addInterceptor(interceptor).build()

                val retrofit = Retrofit.Builder()
                    .baseUrl("https://hacker-news.firebaseio.com/v0/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(ApiService::class.java)
            }
            return retrofitService!!
        }

    }
}