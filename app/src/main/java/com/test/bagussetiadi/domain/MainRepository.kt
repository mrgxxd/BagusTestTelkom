package com.test.bagussetiadi.domain

class MainRepository constructor(private val retrofitService: ApiService) {

    suspend fun getTopStories() = retrofitService.getTopStories()
    suspend fun getDetailStory(id : String) = retrofitService.getDetailStory(id)

}