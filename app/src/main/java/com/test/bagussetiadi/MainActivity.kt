package com.test.bagussetiadi

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.test.bagussetiadi.ui.liststory.ListStoryView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        goToHome()
    }

    fun goToHome(){
        startActivity(Intent(this, ListStoryView::class.java))
    }
}