import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.test.bagussetiadi.R
import com.test.bagussetiadi.ui.detailstory.DetailStoryView

internal class StoriesAdapter(
    private val context: Context,
    private val listStories: List<Integer>
) :
    BaseAdapter() {
    private var layoutInflater: LayoutInflater? = null
    private lateinit var storyId: TextView
    private lateinit var linView: LinearLayout

    override fun getCount(): Int {
        return listStories.size
    }
    override fun getItem(position: Int): Any? {
        return null
    }
    override fun getItemId(position: Int): Long {
        return 0
    }
    override fun getView(
        position: Int,
        convertView: View?,
        parent: ViewGroup
    ): View? {
        var convertView = convertView
        if (layoutInflater == null) {
            layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
        if (convertView == null) {
            convertView = layoutInflater!!.inflate(R.layout.stories_adapter, null)
        }

        linView = convertView!!.findViewById(R.id.linView)
        linView.setOnClickListener {
            val intent = Intent(context, DetailStoryView::class.java)
            intent.putExtra("storyId", listStories[position].toString())
            context.startActivity(intent)
        }

        storyId = convertView!!.findViewById(R.id.storyId)
        storyId.text = listStories[position].toString()

        return convertView
    }
}