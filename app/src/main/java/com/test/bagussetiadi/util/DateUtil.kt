package com.test.bagussetiadi.util

import java.text.SimpleDateFormat
import java.util.*

class DateUtil {
    companion object{
        fun getDateFromInt(date : Long) : String{
            val date = Date(date*1000)
            val format = SimpleDateFormat("dd MMM yyy")
            return format.format(date)
        }
    }
}