package com.test.bagussetiadi.data.model

data class StoryModel(
    val id: Integer?,
    val deleted: Boolean?,
    val type: String?,
    val by: String?,
    val time: Integer?,
    val text: String?,
    val dead: Boolean?,
    val parent: Integer?,
    val poll: Integer?,
    val kids: List<Integer>?,
    val url: String?,
    val score: Integer?,
    val title: String?,
    val parts: List<Integer>?,
    val descendants: Integer?
)