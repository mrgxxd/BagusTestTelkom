package com.test.bagussetiadi.data.local

import android.content.Context
import android.content.SharedPreferences

import android.preference.PreferenceManager




class DataPreferences {

    val KEY_TITLE_STORY = "title-story"

    private fun getSharedPreference(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun setTitleStory(context: Context, title: String?) {
        val editor = getSharedPreference(context).edit()
        editor.putString(KEY_TITLE_STORY, title)
        editor.apply()
    }

    fun getTitleStory(context: Context): String? {
        return getSharedPreference(context).getString(KEY_TITLE_STORY, "")
    }
}