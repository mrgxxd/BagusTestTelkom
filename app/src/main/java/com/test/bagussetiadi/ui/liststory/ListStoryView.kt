package com.test.bagussetiadi.ui.liststory

import StoriesAdapter
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.GridView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.test.bagussetiadi.R
import com.test.bagussetiadi.data.local.DataPreferences
import com.test.bagussetiadi.domain.ApiService
import com.test.bagussetiadi.domain.MainRepository
import com.test.bagussetiadi.ui.MainViewModelFactory


class ListStoryView : AppCompatActivity() {

    lateinit var gridView: GridView
    lateinit var lastFavorite: TextView
    lateinit var progressDialog: ProgressBar

    lateinit var viewModel: ListStoryViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_story)
        initialData()
    }

    override fun onResume() {
        super.onResume()
        val dataPreferences = DataPreferences()
        lastFavorite.text = dataPreferences.getTitleStory(this)
        viewModel.getTopStories()
    }

    fun initialData(){
        gridView = findViewById(R.id.gridView)
        progressDialog = findViewById(R.id.progressDialog)
        lastFavorite = findViewById(R.id.lastFavorite)

        val retrofitService = ApiService.getInstance()
        val mainRepository = MainRepository(retrofitService)
        viewModel = ViewModelProvider(this, MainViewModelFactory(mainRepository)).get(ListStoryViewModel::class.java)

        viewModel.topStoryList.observe(this, {
            Log.d("MASUK DATA", "MASUk DATA")
            val storiesAdapter = StoriesAdapter(this, it!!)
            gridView.adapter = storiesAdapter
        })

        viewModel.errorMessage.observe(this, {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

        viewModel.loading.observe(this, Observer {
            if (it!!) {
                progressDialog.visibility = View.VISIBLE
            } else {
                progressDialog.visibility = View.GONE
            }
        })
    }
}