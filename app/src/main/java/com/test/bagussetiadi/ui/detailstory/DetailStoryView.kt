package com.test.bagussetiadi.ui.detailstory

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.test.bagussetiadi.R
import com.test.bagussetiadi.data.local.DataPreferences
import com.test.bagussetiadi.domain.ApiService
import com.test.bagussetiadi.domain.MainRepository
import com.test.bagussetiadi.ui.MainViewModelFactory
import com.test.bagussetiadi.ui.liststory.ListStoryViewModel
import com.test.bagussetiadi.util.DateUtil

class DetailStoryView : AppCompatActivity() {

    lateinit var storyId : String
    lateinit var viewModel: DetailStoryViewModel

    lateinit var titelStory : TextView
    lateinit var createBy : TextView
    lateinit var createDate : TextView
    lateinit var contentDescription : TextView
    lateinit var imageLike: ImageView
    lateinit var progressDialog: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_story)

        initialData()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getDetailStory(storyId)
    }

    fun initialData(){
        storyId = intent.getStringExtra("storyId").toString()
        titelStory = findViewById(R.id.titleStory)
        createBy = findViewById(R.id.createBy)
        createDate = findViewById(R.id.createDate)
        contentDescription = findViewById(R.id.contentDescription)
        imageLike = findViewById(R.id.imageLike)
        progressDialog = findViewById(R.id.progressDialog)

        imageLike.setOnClickListener {
            Log.d("TEKAN TOMBOL SAVE", "TEKAN TOMBOL SAVE")
            Toast.makeText(this, "Berhasil menyimpan di favorit", Toast.LENGTH_SHORT).show()
            val dataPreferences = DataPreferences()
            dataPreferences.setTitleStory(this, titelStory.text.toString())
        }

        val retrofitService = ApiService.getInstance()
        val mainRepository = MainRepository(retrofitService)
        viewModel = ViewModelProvider(this, MainViewModelFactory(mainRepository)).get(
            DetailStoryViewModel::class.java)

        viewModel.selectedStory.observe(this, {
            Log.d("MASUK DATA", "MASUk DATA")

            titelStory.text = it?.title.toString()
            createBy.text = "by " + it?.by.toString()
            createDate.text = DateUtil.getDateFromInt(it?.time!!.toLong())
            contentDescription.text = it?.text.toString()
        })

        viewModel.errorMessage.observe(this, {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

        viewModel.loading.observe(this, Observer {
            if (it!!) {
                progressDialog.visibility = View.VISIBLE
            } else {
                progressDialog.visibility = View.GONE
            }
        })
    }
}