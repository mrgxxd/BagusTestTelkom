package com.test.bagussetiadi.ui.detailstory

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.test.bagussetiadi.data.model.StoryModel
import com.test.bagussetiadi.domain.MainRepository
import kotlinx.coroutines.*

class DetailStoryViewModel constructor(private val mainRepository: MainRepository) : ViewModel() {

    val errorMessage = MutableLiveData<String>()
    val selectedStory = MutableLiveData<StoryModel>()
    var job: Job? = null
    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }
    val loading = MutableLiveData<Boolean>()

    fun getDetailStory(id : String) {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = mainRepository.getDetailStory(id)
            withContext(Dispatchers.Main) {
                Log.d("RESPONSE DETAIL", response.body().toString())
                if (response.isSuccessful) {
                    selectedStory.postValue(response.body())
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
    }

    private fun onError(message: String) {
        errorMessage.value = message
        loading.value = false
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}