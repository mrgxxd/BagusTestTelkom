package com.test.bagussetiadi.ui

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.test.bagussetiadi.domain.MainRepository
import com.test.bagussetiadi.ui.detailstory.DetailStoryViewModel
import com.test.bagussetiadi.ui.liststory.ListStoryViewModel

class MainViewModelFactory constructor(private val repository: MainRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(ListStoryViewModel::class.java)) {
            ListStoryViewModel(this.repository) as T
        }else if (modelClass.isAssignableFrom(DetailStoryViewModel::class.java)) {
            DetailStoryViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}